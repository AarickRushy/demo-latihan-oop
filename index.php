<?php

require_once('animal.php');
require_once('Frog.php');
require_once('Ape.php');

$sheep = new Animal("Shaun");

echo "Name: " . $sheep->name . "<br>"; // "shaun"
echo "Legs: " . $sheep->legs . "<br>"; // 4
echo "Cold blooded: " . $sheep->cold_blooded . "<br><br>"; // "No"

$kodok = new Frog("Buduk");
echo "Name: " . $kodok->name . "<br>";
echo "Legs: " . $kodok->legs . "<br>"; // 4
echo "Cold blooded: " . $kodok->cold_blooded . "<br>"; // "No"
echo "Jump: " . $kodok->jump() . "<br><br>";

$sungokong = new Ape("Kera Sakti");
echo "Name: " . $sungokong->name . "<br>";
echo "Legs: " . $sungokong->legs . "<br>"; // 4
echo "Cold blooded: " . $sungokong->cold_blooded . "<br>"; // "No"
echo "Yell: " . $sungokong->yell() . "<br><br>";

?>